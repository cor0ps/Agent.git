# Agent

### 介绍
 Java Instrument能做什么？最大的作用？
1 使开发者可以构建一个独立于应用程序的代理程序Agent，用来监控和协助运行在JVM上的程序，更重要的是能够替换和修改某些类的定义；

2 最大的作用：可以实现一种虚拟机级别支持的AOP实现方式；



### 运行

用如下方式运行带有 Instrumentation 的 Java 程序：

```css
java -javaagent:jar文件的位置 [= 传入 premain 的参数 ]
```


### 我的微信号

![qrcode_for_gh_6afac1af7297_344](/images/qrcode_for_gh_6afac1af7297_344.jpg)